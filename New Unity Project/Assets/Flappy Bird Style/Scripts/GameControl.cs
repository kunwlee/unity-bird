﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour 
{
	public static GameControl instance;			
	public Text scoreText;	
	//public GUIText instructionsText;
	//public GUIText pauseText;					
	public GameObject gameOvertext;				

	private int score = 0;					
	public bool gameOver = false;		
	public float scrollSpeed = -1.5f;
	
	private bool pause;
	
	private bool paused = false, waited = true;
	private void waiting()
	{
		Cursor.visible = false;
		if (waited)
		if (Input.GetKeyDown (KeyCode.P)) {
			if (paused)
				paused = false;
			else
				paused = true;
			
			Invoke ("waiting", 0.3f);
		}
	}
	
	void Awake()
	{

		if (instance == null)
			instance = this;
		else if(instance != this)
			Destroy (gameObject);
	}

	void Update()
	{
		if (gameOver && Input.GetMouseButtonDown(0)) 
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

	public void BirdScored()
	{
		if (gameOver)	
			return;
		score++;
		scoreText.text = "Score: " + score.ToString();
	}

	public void BirdDied()
	{
		gameOvertext.SetActive (true);
		gameOver = true;
	}
}
