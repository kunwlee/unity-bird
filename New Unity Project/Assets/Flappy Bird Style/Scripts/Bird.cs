﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour 
{
	public float upForce;                   
	private bool isDead = false;           

	private Animator anim;                  
	private Rigidbody2D rb2d;    
	public AudioClip AC1;
	public AudioClip AC2;
	public AudioClip AC3;

	void Start()
	{
		
		anim = GetComponent<Animator> ();

		rb2d = GetComponent<Rigidbody2D>();
	}
	

	void Update()
	{
		
		if (isDead == false) 
		{

			if (Input.GetMouseButton(0)) 
			{
				
				anim.SetTrigger("Flap");

				//rb2d.velocity = Vector2.zero;
				rb2d.AddForce(new Vector2(0, upForce));

			}

			if (Input.GetMouseButtonDown (0)) {
				GetComponent<AudioSource> ().clip = AC1;
				GetComponent<AudioSource> ().Play ();
			}

			if (Input.GetKey(KeyCode.Space))
			{
				gameObject.layer = 11;//Make it ghost
				GetComponent<AudioSource> ().clip = AC3;
				GetComponent<AudioSource> ().Play ();
			
			}
			else gameObject.layer = 8;
		}

	}

	void OnCollisionEnter2D(Collision2D other)
	{

		rb2d.velocity = Vector2.zero;
		isDead = true;
		anim.SetTrigger ("Die");
		GameControl.instance.BirdDied ();
		GetComponent<AudioSource> ().clip = AC2;
		GetComponent<AudioSource> ().Play ();
	}
}